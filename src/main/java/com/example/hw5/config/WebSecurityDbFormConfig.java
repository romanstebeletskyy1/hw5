package com.example.hw5.config;

import com.example.hw5.filter.JwtFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
@CrossOrigin(origins = {"http://localhost:3000"})
public class WebSecurityDbFormConfig {
    private final JwtFilter jwtFilter;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
//                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeHttpRequests((requests) ->
                        requests
                                .requestMatchers("/auth/login", "/auth/token","/ws/**","/index.html", "/queue/hello.user", "/script.js", "/", "/webjars/**").permitAll()
//                                .requestMatchers("/customers/**", "/employers/**").hasAnyAuthority(JwtUtils.Roles.USER.name())
//                                .requestMatchers("/customers/**", "/employers/**","/accounts/**").hasAnyAuthority(JwtUtils.Roles.ADMIN.name())
                                .anyRequest().authenticated()
                                //.anyRequest().permitAll()
                                .and()
                                .addFilterAfter(jwtFilter, UsernamePasswordAuthenticationFilter.class)

                );

        return http.build();
    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("a")
                .password(passwordEncoder.encode("a"))
                .roles("USER")
                .and()
                .withUser("b")
                .password(passwordEncoder.encode("b"))
                .roles("USER");
    }
}
