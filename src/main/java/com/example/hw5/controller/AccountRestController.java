package com.example.hw5.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.hw5.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/accounts")
@Slf4j
public class AccountRestController {
    @Autowired
    private AccountService accountService;


    @PostMapping("/deposit")
    public ResponseEntity<?> depositMoney(@RequestBody String parametersJson) throws IOException, InterruptedException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumber = nameNodeAccountNumber.get("accountNumber").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());
        log.info("Operation with account: Deposit");
        return accountService.depositAccount(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Depositing of the account is failed");
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawalMoney(@RequestBody String parametersJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumber = nameNodeAccountNumber.get("accountNumber").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());
        log.info("Operation with account: Withdrawal");
        return accountService.withdrawalMoney(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Withdrawing from the account is failed");
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendMoney(@RequestBody String parametersJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNodeAccountNumber = mapper.readTree(parametersJson);
        String accountNumberSender = nameNodeAccountNumber.get("accountNumberSender").asText();
        String accountNumberReceiver = nameNodeAccountNumber.get("accountNumberReceiver").asText();
        Double amount = Double.parseDouble(nameNodeAccountNumber.get("amount").asText());
        log.info("Operation with account: Sending");
        return accountService.sendMoney(accountNumberSender, accountNumberReceiver, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Sending money is failed");
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleExceptions(Exception e) {
        log.warn(e.getMessage());
        return ResponseEntity.status(400).body(e.getMessage());
    }
}
