package com.example.hw5.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.hw5.model.dto.CustomerDtoRequest;
import com.example.hw5.model.dto.CustomerDtoResponse;
import com.example.hw5.service.dtoMapper.CustomerDtoMapperRequest;
import com.example.hw5.service.dtoMapper.CustomerDtoMapperResponse;
import com.example.hw5.model.Currency;
import com.example.hw5.service.CustomerService;
import com.example.hw5.model.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/customers")
@RequiredArgsConstructor
@Slf4j
public class CustomerRestController {

    private final CustomerService customerService;
    private final CustomerDtoMapperResponse customerDtoMapperResponse;
    private final CustomerDtoMapperRequest customerDtoMapperRequest;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        log.info("find all method");
        return ResponseEntity.ok(customerService.getAllCustomers().stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> getAllUsers(@PathVariable Integer page, @PathVariable Integer size) {
        log.info("find all pageable method");
        return ResponseEntity.ok(customerService.getAllCustomers(page, size).stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        log.info("Find by ID method");
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.getCustomerFullInfo(id)));
    }
    @PostMapping("/")
    public CustomerDtoResponse postNewUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        log.info("User created");
        return customerDtoMapperResponse.convertToDto(customerService.createCustomer(customer));
    }
    @PutMapping("/")
    public CustomerDtoResponse updateUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        log.info("User updated");
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.updateCustomer(customer));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        log.info("User deleted");
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String currency) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode newAccountCurrency = objectMapper.readTree(currency);
        String curren = newAccountCurrency.get("currency").asText();
        Currency curr = Currency.forValue(curren);

         Customer customer = customerService.getCustomerFullInfo(id);
         if(customer == null) {
             ResponseEntity.badRequest().body("Customer not found");
         }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.openAccount(customer, curr)));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody String accountNumber ) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.deleteAccount(customer, nameNode.get("accountNumber").asText())));
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationErrors(MethodArgumentNotValidException e){
        List<String> errors = e.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).toList();
        log.warn(getErrorsMap(errors).toString());
        return ResponseEntity.status(400).body(getErrorsMap(errors));
    }


    private Map<String, List<String>> getErrorsMap(List<String> errors) {
        Map<String, List<String>> errorResponse = new HashMap<>();
        errorResponse.put("errors", errors);
        return errorResponse;
    }

}
