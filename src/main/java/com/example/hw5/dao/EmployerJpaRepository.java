package com.example.hw5.dao;

import com.example.hw5.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
