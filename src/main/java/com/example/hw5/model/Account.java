package com.example.hw5.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Table(name = "accounts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "customer")
@EqualsAndHashCode(of={"id"})
public class Account extends AbstractEntity{
    private String number;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance;
    @JsonIgnore
    @ManyToOne
   // @JoinColumn (name = "accounts")
    private Customer customer;
    public Account(Customer customer, Currency currency){
        this.currency = currency;
        this.customer = customer;
        this.number = UUID.randomUUID().toString();
        this.balance = 0D;
    }
/*    @Override
    public String toString() {
        return "Account{" +
                "id=" + this.getId() +
                ", number='" + number + '\'' +
                ", currency=" + currency +
                ", balance=" + balance +
                '}';
    }*/
}
