package com.example.hw5.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employers")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of={"id"})
@ToString(exclude = "customers")
public class Employer extends AbstractEntity {
    private String name;
    private String address;
    @JsonIgnore
    @ManyToMany(mappedBy = "employers")
    private Set<Customer> customers = new HashSet<>();

    public Employer(String name, String address) {
        this.name = name;
        this.address = address;
    }
    public Employer(Long id, String name, String address) {
        this.setId(id);
        this.name = name;
        this.address = address;
    }
    /* @Override
    public String toString() {
        return "Employer{" +
                "id=" + this.getId() +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }*/
}
