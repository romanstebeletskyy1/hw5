package com.example.hw5.model;

import lombok.Data;

@Data
public class HelloMessage {
    private String name;
}
