package com.example.hw5.model.dto;

import com.example.hw5.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDtoResponse {

    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private Long userId;
}
