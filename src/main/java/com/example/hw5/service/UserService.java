package com.example.hw5.service;

import com.example.hw5.dao.UserJpaRepository;
import com.example.hw5.model.SysUser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserJpaRepository userJpaRepository;

    public Optional<SysUser> getByLogin(@NonNull String login) {

        return userJpaRepository.findUsersByUserName(login);
    }

}