package com.example.hw5.service.dtoMapper;

import com.example.hw5.model.Account;
import com.example.hw5.model.dto.AccountDtoRequest;
import com.example.hw5.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperRequest extends DtoMapperFacade<Account, AccountDtoRequest> {

    public AccountDtoMapperRequest() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setNumber(dto.getNumber());
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}
