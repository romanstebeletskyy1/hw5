package com.example.hw5.service.dtoMapper;

import com.example.hw5.model.Customer;
import com.example.hw5.model.dto.CustomerDtoRequest;
import com.example.hw5.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperRequest extends DtoMapperFacade<Customer, CustomerDtoRequest> {

    public CustomerDtoMapperRequest() {
        super(Customer.class, CustomerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Customer entity, CustomerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAge(dto.getAge());
        entity.setEmail(dto.getEmail());
        entity.setPhone(dto.getPhone());
    }
}
