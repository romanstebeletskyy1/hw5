package com.example.hw5.service.dtoMapper;

import com.example.hw5.model.Employer;
import com.example.hw5.model.dto.EmployerDtoRequest;
import com.example.hw5.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }
}
